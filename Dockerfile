FROM igwn/lalsuite-dev:buster

LABEL name="LALSuite Development - GCC 7"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Unsupported"

# install dependencies
RUN apt-get update && apt-get --assume-yes install \
        g++-7 \
        gcc-7 && \
    apt-get clean

# set gcc-7 as the default compiler
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 --slave /usr/bin/g++ g++ /usr/bin/g++-7

# git-lfs post-install
RUN git lfs install
